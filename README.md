# Inflation adjusted Hacker News Web-Extension

This extension automatically displays the inflation adjusted score next to the actual score of any Hacker News story.

The extension is compatible with Firefox and Chrome. Download link for Firefox: https://addons.mozilla.org/en-US/firefox/addon/inflation-adjusted-hacker-news/

Find out more about inflation adjusted Hacker News: https://instruments.digital/inflation-adjusted-hn/
