/**
 * Converts csv string to inflation data object
 * @param strData csv string containing inflation data
 * @returns {{averages: *[], dates: *[]}}
 */
function csvToInflationData(strData) {
    const objPattern = new RegExp(("(\\,|\\r?\\n|\\r|^)(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|([^\\,\\r\\n]*))"),"gi");
    let arrMatches = null, arrData = [[]];
    while (arrMatches = objPattern.exec(strData)){
        if (arrMatches[1].length && arrMatches[1] !== ",")arrData.push([]);
        arrData[arrData.length - 1].push(arrMatches[2] ?
            arrMatches[2].replace(new RegExp( "\"\"", "g" ), "\"") :
            arrMatches[3]);
    }

    arrData = arrData.slice(0, -1); // Remove the last line because it's always empty

    return {
        dates: arrData.map(item => item[0]),
        averages: arrData.map(item => item[1]),
    };
}

/**
 * Loads inflation data from either cache or instruments.digital
 * @returns {Promise<*>}
 */
async function loadInflationData() {
    const today = new Date().setHours(0, 0, 0, 0)

    // First we check whether inflation data is already in cache
    if (localStorage.hasOwnProperty('inflationData')) {
        if (localStorage.inflationData.loadDate >= today) {
            return JSON.parse(localStorage.inflationData).data
        }
    }

    // If the data was not in the cache, we fetch it from instrument.digital
    const csv = await (await fetch('https://instruments.digital/inflation-adjusted-hn/data.csv')).text()
    const inflationData = csvToInflationData(csv)

    // Write the data to the cache
    localStorage.inflationData = JSON.stringify({
        loadDate: today,
        data: inflationData
    })

    // And finally return the inflation data
    return JSON.parse(localStorage.inflationData).data
}

/**
 * Fetches one Hacker News story from cache or Hacker News API
 * @param storyId
 * @returns {Promise<any>}
 */
async function fetchStory(storyId) {
    // The index includes the current hour since 1970 so that the cache automatically invalidates after an hour on average
    const currentHour = Math.floor(new Date().getTime() / 1000 / 60 / 60)
    const storyIndex = `story:${storyId}:${currentHour}`

    // Check if story is already in cache and if so, return it
    if (localStorage.hasOwnProperty(storyIndex)) {
        return JSON.parse(localStorage[storyIndex]);
    }

    // Else fetch story from Hacker News API and write it to cache
    const {score, time} = await (await fetch(`https://hacker-news.firebaseio.com/v0/item/${storyId}.json`)).json()
    localStorage[storyIndex] = JSON.stringify({score, time});

    // Finally return it
    return JSON.parse(localStorage[storyIndex]);
}

/**
 * Main function of the script
 * @returns {Promise<void>}
 */
async function main() {
    // First we get the inflation data
    const inflationData = await loadInflationData();

    // This selects all stories in page
    const allItems = document.querySelectorAll('.itemlist tr[id], .fatitem tr[id]')

    // We iterate all stories so we display the inflation adjusted score for each story
    for (let item of allItems) {
        const storyId = item.id;
        const story = await fetchStory(storyId)
        // This line converts the timestamp to the YYYY-MM-DD format which is used in the inflation dataset
        let storyDate = (new Date(story.time * 1000)).toISOString().substr(0, 10)

        // If the story is newer than the inflation data we will just use the newest data we have
        const newestDateInData = new Date(inflationData.dates[inflationData.dates.length - 1]).getTime();
        if (story.time * 1000 > newestDateInData) {
            storyDate = inflationData.dates[inflationData.dates.length - 1]
        }

        // The following lines calculate the inflation and the adjusted score
        const dateKey = inflationData.dates.findIndex(date => date === storyDate)
        const avgScore = inflationData.averages[dateKey]
        const currentAvgScore = inflationData.averages[inflationData.averages.length - 1]
        const adjustedPoints = Math.round(story.score * (currentAvgScore / avgScore))

        // Now we can create a new element to display the adjusted score
        const elem = document.createElement('span');
        elem.innerText = ` (${adjustedPoints} points)`;
        elem.style.color = 'darkblue';
        elem.title = 'Inflation Adjusted Points'

        const scoreElement = item.nextElementSibling.querySelector('.score');
        if (!scoreElement) { // Some stories such as job listings don't have a score, we just skip those
            continue;
        }
        scoreElement.parentNode.insertBefore(elem, scoreElement.nextSibling);
    }
}

main()